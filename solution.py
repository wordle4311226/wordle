import requests as rq
from typing import Self
import random
DEBUG = False

class wordlebot:
    words = [word.strip() for word in open("words.txt")]
    wordle_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = wordle_url + "register"
    create_url = wordle_url + "create"
    guess_url = wordle_url + "guess"
    SINGLETON = True

    def __init__(self: Self, name: str, trials: int):
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(wordlebot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        create_dict = {"id": self.me, "overwrite": True}
        self.session.post(wordlebot.create_url, json=create_dict)
        self.trials = trials
        self.choices = [w for w in wordlebot.words[:]]
        random.shuffle(self.choices)
        self.result = {"Y": [], "R": [], "G": []}
        if DEBUG:
            print(self.choices)

    def play(self: Self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(wordlebot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']

        while self.trials != 0 and not won:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(feedback, choice)
            self.trials -= 1
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
        if(self.trials != 0):
            print("Secret is", choice, "found in", len(tries), "attempts")
            print("Route is:", " => ".join(tries))
        
        if(self.trials == 0 and not won) :
            print(" You lost the game ! ")
            
    def update(self: Self, feedback: str, choice: str):
       
    
        for i, j in zip(feedback, choice):
            if i == 'Y':
                self.result["Y"].append(j)
            elif i == 'G':
                self.result["G"].append((j, choice.index(j)))
            elif i == 'R':
                self.result["R"].append(j)
        
        def common(word: str):
            return all(letter in word for letter in self.result["Y"]) and all(letter not in word for letter in self.result["R"])

        def correct_pos(word: str):
            return all(word[idx] == letter for letter, idx in self.result["G"])

        self.choices = [word for word in self.choices if common(word) and correct_pos(word)]
        if DEBUG:
            print(self.result["Y"], self.result["R"], self.result["G"])
            print(self.choices[:10])

game = wordlebot("Khushi", 6)
game.play()
