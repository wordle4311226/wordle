import requests as rq
import random
import time
class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.Session()
        register_dict = {"mode": "mastermind", "name": name}
        reg_resp = self.session.post(self.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(self.creat_url, json=creat_dict)

        self.choices = [w for w in self.words[:] if is_unique(w) and len(w) == 5]
        random.shuffle(self.choices)

    def get_feedback(self, feedback: int) -> str:
        if feedback == 2:
            return "G"  # Green (in word, correct position)
        elif feedback == 1:
            return "Y"  # Yellow (in word, wrong position)
        elif feedback == 0:
            return "R"  # Red (not in word)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(self.guess_url, json=guess)
            rj = response.json()
            feedback = self.get_feedback(rj["feedback"])
            won = "win" in rj["message"]
            return feedback, won

        for _ in range(6):
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            print(f"Guess: {choice}, Feedback: {feedback}")
            time.sleep(1)
            if won:
                print("You Won!")
                return
        print("Game over. You didn't win.")

game = MMBot("CodeShifu")
game.play()
